<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Domain\Model;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractValueObject;

/**
 * Group of historical events in the chronicle.
 */
class Eventgroup extends AbstractValueObject
{

    /**
     * Name of the event group.
     * 
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $name = '';
    
    /**
     * Returns the name
     * 
     * @return string name
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

}
