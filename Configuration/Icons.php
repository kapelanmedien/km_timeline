<?php

return [
    'km-timeline' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:km_timeline/Resources/Public/Icons/Extension.svg',
    ],
];
