<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Domain\Model;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use KapelanMedien\KmTimeline\Domain\Model\Eventgroup;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;

/**
 * Historical event in the chronicle.
 */
class Event extends AbstractEntity
{

    /**
     * Title of the event.
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $headline = '';

    /**
     * Description text for the event.
     *
     * @var string
     */
    protected $text = '';

    /**
     * Year in which the event started.
     *
     * @var int
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $startYear = null;

    /**
     * Month in which the event started.
     *
     * @var int
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $startMonth = 0;

    /**
     * Day in which the event started.
     *
     * @var int
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $startDay = 0;

    /**
     * Time in which the event started.
     *
     * @var \DateTime
     */
    protected $startTime = null;

    /**
     * Year in which the event ended.
     *
     * @var int
     */
    protected $endYear = null;

    /**
     * Month in which the event ended.
     *
     * @var int
     */
    protected $endMonth = 0;

    /**
     * Day in which the event ended.
     *
     * @var int
     */
    protected $endDay = 0;

    /**
     * Time in which the event ended.
     *
     * @var \DateTime
     */
    protected $endTime = null;

    /**
     * Date format for display.
     *
     * @var int
     */
    protected $dateFormat = 0;

    /**
     * URL to external media shown with the event.
     *
     * @var string
     */
    protected $mediaUrl = '';

    /**
     * Image shown with the event. Is used as thumbnail if URL to external media is set.
     *
     * @var FileReference|null
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $media = null;

    /**
     * Group/ category of events to which the event belongs.
     *
     * @var Eventgroup
     */
    protected $eventgroup = null;

    /**
     * Returns the headline
     *
     * @return string headline
     */
    public function getHeadline(): string
    {
        return $this->headline;
    }

    /**
     * Sets the headline
     *
     * @param string $headline
     * @return void
     */
    public function setHeadline($headline): void
    {
        $this->headline = $headline;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text): void
    {
        $this->text = $text;
    }

    /**
     * Returns the startYear
     *
     * @var string
     * @return int startYear
     */
    public function getStartYear(): int
    {
        return $this->startYear;
    }

    /**
     * Sets the startYear
     *
     * @param int $startYear
     * @return void
     */
    public function setStartYear($startYear): void
    {
        $this->startYear = $startYear;
    }

    /**
     * Returns the startMonth
     *
     * @return int $startMonth
     */
    public function getStartMonth(): int
    {
        return $this->startMonth;
    }

    /**
     * Sets the startMonth
     *
     * @param int $startMonth
     * @return void
     */
    public function setStartMonth($startMonth): void
    {
        $this->startMonth = $startMonth;
    }

    /**
     * Returns the startDay
     *
     * @return int $startDay
     */
    public function getStartDay(): int
    {
        return $this->startDay;
    }

    /**
     * Sets the startDay
     *
     * @param int $startDay
     * @return void
     */
    public function setStartDay($startDay): void
    {
        $this->startDay = $startDay;
    }

    /**
     * Returns the startTime
     *
     * @return \DateTime|null $startTime
     */
    public function getStartTime(): ?\DateTime
    {
        $startTimeTZ = $this->startTime;
        if ($startTimeTZ != null) {
            $startTimeTZ->setTimezone(new \DateTimeZone('UTC'));
        }
        
        return $startTimeTZ;
//         return $this->startTime;
    }

    /**
     * Sets the startTime
     *
     * @param \DateTime $startTime
     * @return void
     */
    public function setStartTime(\DateTime $startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * Returns the endYear
     *
     * @return int|null endYear
     */
    public function getEndYear(): ?int
    {
        return $this->endYear;
    }

    /**
     * Sets the endYear
     *
     * @param int $endYear
     * @return void
     */
    public function setEndYear($endYear): void
    {
        $this->endYear = $endYear;
    }

    /**
     * Returns the endMonth
     *
     * @return int $endMonth
     */
    public function getEndMonth(): int
    {
        return $this->endMonth;
    }

    /**
     * Sets the endMonth
     *
     * @param int $endMonth
     * @return void
     */
    public function setEndMonth($endMonth): void
    {
        $this->endMonth = $endMonth;
    }

    /**
     * Returns the endDay
     *
     * @return int $endDay
     */
    public function getEndDay(): int
    {
        return $this->endDay;
    }

    /**
     * Sets the endDay
     *
     * @param int $endDay
     * @return void
     */
    public function setEndDay($endDay): void
    {
        $this->endDay = $endDay;
    }

    /**
     * Returns the endTime
     *
     * @return \DateTime|null $endTime
     */
    public function getEndTime(): ?\DateTime
    {
        $endTimeTZ = $this->endTime;
        if ($endTimeTZ != null) {
            $endTimeTZ->setTimezone(new \DateTimeZone('UTC'));
        }
        
        return $endTimeTZ;
//         return $this->endTime;
    }

    /**
     * Sets the endTime
     *
     * @param \DateTime $endTime
     * @return void
     */
    public function setEndTime(\DateTime $endTime): void
    {
        $this->endTime = $endTime;
    }

    /**
     * Returns the dateFormat
     *
     * @return int $dateFormat
     */
    public function getDateFormat(): int
    {
        return $this->dateFormat;
    }

    /**
     * Sets the dateFormat
     *
     * @param int $dateFormat
     * @return void
     */
    public function setDateFormat($dateFormat): void
    {
        $this->dateFormat = $dateFormat;
    }

    /**
     * Returns the mediaUrl
     *
     * @return string $mediaUrl
     */
    public function getMediaUrl(): string
    {
        return $this->mediaUrl;
    }

    /**
     * Sets the mediaUrl
     *
     * @param string $mediaUrl
     * @return void
     */
    public function setMediaUrl($mediaUrl): void
    {
        $this->mediaUrl = $mediaUrl;
    }

    /**
     * Returns the media
     *
     * @return FileReference|null $media
     */
    public function getMedia(): ?FileReference
    {
        if ($this->media instanceof LazyLoadingProxy) {
            /** @var FileReference $media */
            $media = $this->media->_loadRealInstance();
            $this->media = $media;
        }
        
        return $this->media;
    }

    /**
     * Sets the media
     *
     * @param FileReference $media
     * @return void
     */
    public function setMedia(FileReference $media): void
    {
        $this->media = $media;
    }

    /**
     * Returns the eventgroup
     *
     * @return Eventgroup|null $eventgroup
     */
    public function getEventgroup(): ?Eventgroup
    {
        return $this->eventgroup;
    }

    /**
     * Sets the eventgroup
     *
     * @param Eventgroup $eventgroup
     * @return void
     */
    public function setEventgroup(Eventgroup $eventgroup): void
    {
        $this->eventgroup = $eventgroup;
    }

}
