<?php
defined('TYPO3') or die();

(function () {
    // Configure frontend plugin
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'KmTimeline',
        'TimelineJs',
        [
            \KapelanMedien\KmTimeline\Controller\TimelineController::class => 'show',
        ]
    );
    
    // Add upgrade wizard
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['kmTimeline_pluginUpdater'] = \KapelanMedien\KmTimeline\Updates\PluginUpdater::class;
    
    // Add default PageTsConfig
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:km_timeline/Configuration/TsConfig/NewContentElementWizard.tsconfig">');
})();
