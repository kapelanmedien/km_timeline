<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Controller;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use KapelanMedien\KmTimeline\Domain\Model\Timeline;
use KapelanMedien\KmTimeline\Domain\Repository\EraRepository;
use KapelanMedien\KmTimeline\Domain\Repository\EventRepository;
use KapelanMedien\KmTimeline\Domain\Repository\TimelineRepository;
use KapelanMedien\KmTimeline\Utility\TypoScript;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Pagination\SimplePagination;
use TYPO3\CMS\Extbase\Pagination\QueryResultPaginator;

/**
 * TimelineController
 */
class TimelineController extends ActionController
{

    /**
     * timelineRepository
     *
     * @var TimelineRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $timelineRepository = null;

    /**
     * eventsDefaultDateFormat
     *
     * @var string
     */
    protected $eventsDefaultDateFormat = null;

    /**
     * erasDefaultDateFormat
     *
     * @var string
     */
    protected $erasDefaultDateFormat = null;
    
    /**
     * TimelineController constructor.
     * @param TimelineRepository $timelineRepository
     */
    public function __construct(TimelineRepository $timelineRepository)
    {
        $this->timelineRepository = $timelineRepository;
    }

    /**
     * @return void
     */
    public function initializeAction(): void
    {
        $this->buildSettings();
        if (ApplicationType::fromRequest($this->request)->isBackend()) {
//             $this->pageId = (int) GeneralUtility::_GP('id');
            $this->pageId = $this->request->getParsedBody()['id'] ?? $this->request->getQueryParams()['id'] ?? null;
            $frameworkConfiguration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
            $persistenceConfiguration = ['persistence' => ['storagePid' => $this->pageId]];
            $this->configurationManager->setConfiguration(array_merge($frameworkConfiguration, $persistenceConfiguration));
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction(): void
    {
        if (ApplicationType::fromRequest($this->request)->isBackend()) {
            $config = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
            $storagePid = $config['persistence']['storagePid'];
            $timelines = $this->timelineRepository->findAll();
            
            $currentPage = 1;
            if ($this->request->hasArgument('currentPage')) {
                $currentPage =  $this->request->getArgument('currentPage');
            }
            $itemsPerPage = $this->settings['perPage'] ?? 10;
            $paginator = new QueryResultPaginator($timelines, (int)$currentPage, (int)$itemsPerPage);
            $pagination = new SimplePagination($paginator);
            
            $this->view->assignMultiple([
//                 'timelines' => $paginator->getPaginatedItems(),
                'storagePid' => $storagePid,
                'pagination' => [
                    'pagination' => $pagination,
                    'paginator' => $paginator,
                ],
            ]);
        }
    }

    /**
     * action show
     *
     * @param Timeline $timeline
     * @return void
     */
    public function showAction(Timeline $timeline = null): void
    {
        if (ApplicationType::fromRequest($this->request)->isFrontend()) {
            // if timeline is not set try predefined
            $timeline = $timeline ?? $this->timelineRepository->findByUid($this->settings['timeline']);
            if (isset($timeline)) {
                $this->view->assign('timeline', $timeline);
                /* get timeline as JSON object */
                // thumbnail settings
                $thumbnailMaxHeight = 30;
                $thumbnailMaxWidth = 40;
                if (!empty($this->settings['thumbnail'])) {
                    $thumbnailMaxHeight = empty($this->settings['thumbnail']['maxHeight']) ? $thumbnailMaxHeight : $this->settings['thumbnail']['maxHeight'];
                    $thumbnailMaxWidth = empty($this->settings['thumbnail']['maxWidth']) ? $thumbnailMaxWidth : $this->settings['thumbnail']['maxWidth'];
                }
                /** @var ImageService $imageService */
                $imageService = GeneralUtility::makeInstance(ImageService::class);
                $processingInstructions = ['maxWidth' => $thumbnailMaxWidth, 'maxHeight' => $thumbnailMaxHeight];

                $timelineEvents = $timeline->getEvents();
                $timelineTitleEvent = $timeline->getTitle();
                $timelineEras = $timeline->getEras();
                $timelineObjects = [];
                $events = [];
                foreach ($timelineEvents as $timelineEvent) {
                    $timelineEventArray = [];
                    $dateFormat = empty($timelineEvent->getDateFormat()) ? $this->eventsDefaultDateFormat : LocalizationUtility::translate('tx_kmtimeline.date_format.' . $timelineEvent->getDateFormat(), 'KmTimeline');
                    $timelineEventArray['start_date'] = empty($timelineEvent->getStartYear()) ? '' : ['year' => $timelineEvent->getStartYear(), 'month' => $timelineEvent->getStartMonth(), 'day' => $timelineEvent->getStartDay()];
                    $startTime = $timelineEvent->getStartTime();
                    if (!empty($startTime)) {
                        $timelineEventArray['start_date']['hour'] = $startTime->format('H');
                        $timelineEventArray['start_date']['minute'] = $startTime->format('i');
                    }
                    if (!empty($dateFormat)) {
                        $timelineEventArray['start_date']['format'] = $dateFormat;
                    }
                    if (!empty($timelineEvent->getEndYear())) {
                        $timelineEventArray['end_date'] = empty($timelineEvent->getEndYear()) ? '' : ['year' => $timelineEvent->getEndYear(), 'month' => $timelineEvent->getEndMonth(), 'day' => $timelineEvent->getEndDay()];
                        $endTime = $timelineEvent->getEndTime();
                        if (!empty($endTime)) {
                            $timelineEventArray['end_date']['hour'] = $endTime->format('H');
                            $timelineEventArray['end_date']['minute'] = $endTime->format('i');
                        }
                        if (!empty($dateFormat)) {
                            $timelineEventArray['end_date']['format'] = $dateFormat;
                        }
                    }
                    $timelineEventArray['text'] = ['headline' => $timelineEvent->getHeadline(), 'text' => $timelineEvent->getText()];
                    if (!empty($timelineEvent->getMediaUrl())) {
                        $timelineEventArray['media']['url'] = $timelineEvent->getMediaUrl();
                        if (!empty($timelineEvent->getMedia())) {
                            // process thumbnail
                            $thumbnail = $imageService->applyProcessingInstructions($timelineEvent->getMedia()->getOriginalResource(), $processingInstructions);
                            $timelineEventArray['media']['thumbnail'] = $imageService->getImageUri($thumbnail);
                        }
                    } elseif (!empty($timelineEvent->getMedia())) {
                        $mediaFile = $timelineEvent->getMedia()->getOriginalResource();
                        $timelineEventArray['media']['url'] = $mediaFile->getPublicUrl();
                        $timelineEventMediaDescription = $mediaFile->getDescription();
                        if (!is_null($timelineEventMediaDescription)) {
                            $timelineEventArray['media']['caption'] = $timelineEventMediaDescription;
                        }
                        $timelineEventMediaTitle = $mediaFile->getTitle();
                        if (!is_null($timelineEventMediaTitle)) {
                            $timelineEventArray['media']['credit'] = $timelineEventMediaTitle;
                        }
                        // process thumbnail
                        $thumbnail = $imageService->applyProcessingInstructions($mediaFile, $processingInstructions);
                        $timelineEventArray['media']['thumbnail'] = $imageService->getImageUri($thumbnail);
                    }
                    if (!empty($timelineEvent->getEventgroup())) {
                        $timelineEventArray['group'] = $timelineEvent->getEventgroup()->getName();
                    }
                    $events[] = $timelineEventArray;
                }
                if (!empty($events)) {
                    $timelineObjects['events'] = $events;
                }
                if ($timelineTitleEvent) {
                    $timelineTitle = [];
                    $timelineTitle['text'] = ['headline' => $timelineTitleEvent->getHeadline(), 'text' => $timelineTitleEvent->getText()];
                    if (!empty($timelineTitleEvent->getMediaUrl())) {
                        $timelineTitle['media']['url'] = $timelineTitleEvent->getMediaUrl();
                        if (!empty($timelineTitleEvent->getMedia())) {
                            // process thumbnail
                            $titleThumbnail = $imageService->applyProcessingInstructions($timelineTitleEvent->getMedia()->getOriginalResource(), $processingInstructions);
                            $timelineTitle['media']['thumbnail'] = $imageService->getImageUri($titleThumbnail);
                        }
                    } elseif (!empty($timelineTitleEvent->getMedia())) {
                        $titleMediaFile = $timelineTitleEvent->getMedia()->getOriginalResource();
                        $timelineTitle['media']['url'] = $titleMediaFile->getPublicUrl();
                        $timelineTitleEventMediaDescription = $titleMediaFile->getDescription();
                        if (!is_null($timelineTitleEventMediaDescription)) {
                            $timelineTitle['media']['caption'] = $timelineTitleEventMediaDescription;
                        }
                        $timelineTitleEventMediaTitle = $titleMediaFile->getTitle();
                        if (!is_null($timelineTitleEventMediaTitle)) {
                            $timelineTitle['media']['credit'] = $timelineTitleEventMediaTitle;
                        }
                        // process thumbnail
                        $titleThumbnail = $imageService->applyProcessingInstructions($titleMediaFile, $processingInstructions);
                        $timelineTitle['media']['thumbnail'] = $imageService->getImageUri($titleThumbnail);
                    }
                    $timelineObjects['title'] = $timelineTitle;
                }
                $eras = [];
                foreach ($timelineEras as $timelineEra) {
                    $timelineEraArray = [];
                    $dateFormat = empty($timelineEra->getDateFormat()) ? $this->erasDefaultDateFormat : LocalizationUtility::translate('tx_kmtimeline.date_format.' . $timelineEra->getDateFormat(), 'KmTimeline');
                    $timelineEraArray['start_date'] = empty($timelineEra->getStartYear()) ? '' : ['year' => $timelineEra->getStartYear(), 'month' => $timelineEra->getStartMonth(), 'day' => $timelineEra->getStartDay()];
                    $startTime = $timelineEra->getStartTime();
                    if (!empty($startTime)) {
                        $timelineEraArray['start_date']['hour'] = $startTime->format('H');
                        $timelineEraArray['start_date']['minute'] = $startTime->format('i');
                    }
                    if (!empty($dateFormat)) {
                        $timelineEraArray['start_date']['format'] = $dateFormat;
                    }
                    $timelineEraArray['end_date'] = empty($timelineEra->getEndYear()) ? '' : ['year' => $timelineEra->getEndYear(), 'month' => $timelineEra->getEndMonth(), 'day' => $timelineEra->getEndDay()];
                    $endTime = $timelineEra->getEndTime();
                    if (!empty($endTime)) {
                        $timelineEraArray['end_date']['hour'] = $endTime->format('H');
                        $timelineEraArray['end_date']['minute'] = $endTime->format('i');
                    }
                    if (!empty($dateFormat)) {
                        $timelineEraArray['end_date']['format'] = $dateFormat;
                    }
                    $timelineEraArray['text'] = ['headline' => $timelineEra->getHeadline(), 'text' => ''];
                    $eras[] = $timelineEraArray;
                }
                if (!empty($eras)) {
                    $timelineObjects['eras'] = $eras;
                }
                $this->view->assign('timelineJson', json_encode($timelineObjects));
                // settings
                $this->view->assign('sysLanguage', $GLOBALS['TYPO3_REQUEST']->getAttribute('language')->getTwoLetterIsoCode());
            }
        } /* elseif (ApplicationType::fromRequest($this->request)->isBackend()) {
            $config = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
            $storagePid = $config['persistence']['storagePid'];
            $this->view->assign('storagePid', $storagePid);
            $events = $this->eventRepository->findByTimeline($timeline->getUid());
            $this->view->assign('events', $events);
            $eras = $this->eraRepository->findByTimeline($timeline->getUid());
            $this->view->assign('eras', $eras);
            $this->view->assign('timeline', $timeline);
        } */
    }
    
    /**
     * @return void
     */
    public function buildSettings(): void
    {
        $settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
        $defaultDateFormat = empty($settings['defaultDateFormat']['_typoScriptNodeValue']) ? '' : (is_numeric($settings['defaultDateFormat']['_typoScriptNodeValue']) ? LocalizationUtility::translate('tx_kmtimeline.date_format.' . $settings['defaultDateFormat']['_typoScriptNodeValue'], 'KmTimeline') : $settings['defaultDateFormat']['_typoScriptNodeValue']);
        unset($settings['defaultDateFormat']['_typoScriptNodeValue']);
        if (isset($settings['override']) && is_array($settings['override'])) {
            $overrides = $settings['override'];
            unset($settings['override']);
            $typoScriptUtility = GeneralUtility::makeInstance(TypoScript::class);
            $settings['merged'] = $typoScriptUtility->override($settings, $overrides);
        }
        $this->settings = $settings['merged'] ?? $settings;
        $this->eventsDefaultDateFormat = empty($this->settings['defaultDateFormat']['events']) ? $defaultDateFormat : (is_numeric($this->settings['defaultDateFormat']['events']) ? LocalizationUtility::translate('tx_kmtimeline.date_format.' . $this->settings['defaultDateFormat']['events'], 'KmTimeline') : $this->settings['defaultDateFormat']['events']);
        $this->settings['defaultDateFormat']['events'] = $this->eventsDefaultDateFormat;
        $this->erasDefaultDateFormat = empty($this->settings['defaultDateFormat']['eras']) ? $defaultDateFormat : (is_numeric($this->settings['defaultDateFormat']['eras']) ? LocalizationUtility::translate('tx_kmtimeline.date_format.' . $this->settings['defaultDateFormat']['eras'], 'KmTimeline') : $this->settings['defaultDateFormat']['eras']);
        $this->settings['defaultDateFormat']['eras'] = $this->erasDefaultDateFormat;
    }

}
