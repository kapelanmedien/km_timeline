.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _start:

=============================================================
###PROJECT_NAME### (Deutsch)
=============================================================

.. only:: html

	:Klassifikation:
		km_timeline

	:Version:
		|release|

	:Sprache:
		de

	:Beschreibung:
		Erstellt eine Zeitleiste einer Chronik.

	:Schlüsselwörter:
		Zeitleiste,Chronik

	:Copyright:
		###YEAR###

	:Autor:
		###AUTHOR###

	:E-Mail:
		wiebach@kapelan.com

	:Lizenz:
		Dieses Dokument wird unter der Open Publication License, siehe
		http://www.opencontent.org/openpub/ veröffentlicht.

	:Gerendert:
		|today|

	Der Inhalt dieses Dokuments bezieht sich auf TYPO3,
	ein GNU/GPL CMS-Framework auf `www.typo3.org <https://typo3.org/>`__.


	**Inhaltsverzeichnis**

.. toctree::
	:maxdepth: 3
	:titlesonly:

..	Introduction/Index
..	UserManual/Index
..	AdministratorManual/Index
..	Configuration/Index
..	DeveloperCorner/Index
..	KnownProblems/Index
..	ToDoList/Index
..	ChangeLog/Index
