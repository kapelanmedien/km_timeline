<?php

namespace KapelanMedien\KmTimeline\Tests\Unit\Domain\Model;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case for class \KapelanMedien\KmTimeline\Domain\Model\Timeline.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Uwe Wiebach <wiebach@kapelan.com>
 */
class TimelineTest extends UnitTestCase
{
    /**
     * @var \KapelanMedien\KmTimeline\Domain\Model\Timeline
     */
    protected $subject = null;

    public function setUp()
    {
        $this->subject = new \KapelanMedien\KmTimeline\Domain\Model\Timeline();
    }

    public function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        $this->assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        $this->assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString()
    {
        $this->assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription()
    {
        $this->subject->setDescription('Conceived at T3CON10');

        $this->assertAttributeEquals(
            'Conceived at T3CON10',
            'description',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForEvent()
    {
        $this->assertEquals(
            null,
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForEventSetsTitle()
    {
        $titleFixture = new \KapelanMedien\KmTimeline\Domain\Model\Event();
        $this->subject->setTitle($titleFixture);

        $this->assertAttributeEquals(
            $titleFixture,
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEventsReturnsInitialValueForEvent()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->assertEquals(
            $newObjectStorage,
            $this->subject->getEvents()
        );
    }

    /**
     * @test
     */
    public function setEventsForObjectStorageContainingEventSetsEvents()
    {
        $event = new \KapelanMedien\KmTimeline\Domain\Model\Event();
        $objectStorageHoldingExactlyOneEvents = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneEvents->attach($event);
        $this->subject->setEvents($objectStorageHoldingExactlyOneEvents);

        $this->assertAttributeEquals(
            $objectStorageHoldingExactlyOneEvents,
            'events',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addEventToObjectStorageHoldingEvents()
    {
        $event = new \KapelanMedien\KmTimeline\Domain\Model\Event();
        $eventsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', ['attach'], [], '', false);
        $eventsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($event));
        $this->inject($this->subject, 'events', $eventsObjectStorageMock);

        $this->subject->addEvent($event);
    }

    /**
     * @test
     */
    public function removeEventFromObjectStorageHoldingEvents()
    {
        $event = new \KapelanMedien\KmTimeline\Domain\Model\Event();
        $eventsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', ['detach'], [], '', false);
        $eventsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($event));
        $this->inject($this->subject, 'events', $eventsObjectStorageMock);

        $this->subject->removeEvent($event);

    }

    /**
     * @test
     */
    public function getErasReturnsInitialValueForEra()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->assertEquals(
            $newObjectStorage,
            $this->subject->getEras()
        );
    }

    /**
     * @test
     */
    public function setErasForObjectStorageContainingEraSetsEras()
    {
        $era = new \KapelanMedien\KmTimeline\Domain\Model\Era();
        $objectStorageHoldingExactlyOneEras = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneEras->attach($era);
        $this->subject->setEras($objectStorageHoldingExactlyOneEras);

        $this->assertAttributeEquals(
            $objectStorageHoldingExactlyOneEras,
            'eras',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addEraToObjectStorageHoldingEras()
    {
        $era = new \KapelanMedien\KmTimeline\Domain\Model\Era();
        $erasObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', ['attach'], [], '', false);
        $erasObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($era));
        $this->inject($this->subject, 'eras', $erasObjectStorageMock);

        $this->subject->addEra($era);
    }

    /**
     * @test
     */
    public function removeEraFromObjectStorageHoldingEras()
    {
        $era = new \KapelanMedien\KmTimeline\Domain\Model\Era();
        $erasObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', ['detach'], [], '', false);
        $erasObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($era));
        $this->inject($this->subject, 'eras', $erasObjectStorageMock);

        $this->subject->removeEra($era);

    }

    /**
     * @test
     */
    public function getEventgroupsReturnsInitialValueForEventgroup()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->assertEquals(
            $newObjectStorage,
            $this->subject->getEventgroups()
        );
    }

    /**
     * @test
     */
    public function setEventgroupsForObjectStorageContainingEventgroupSetsEventgroups()
    {
        $eventgroup = new \KapelanMedien\KmTimeline\Domain\Model\Eventgroup();
        $objectStorageHoldingExactlyOneEventgroups = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneEventgroups->attach($eventgroup);
        $this->subject->setEventgroups($objectStorageHoldingExactlyOneEventgroups);

        $this->assertAttributeEquals(
            $objectStorageHoldingExactlyOneEventgroups,
            'eventgroups',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addEventgroupToObjectStorageHoldingEventgroups()
    {
        $eventgroup = new \KapelanMedien\KmTimeline\Domain\Model\Eventgroup();
        $eventgroupsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', ['attach'], [], '', false);
        $eventgroupsObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($eventgroup));
        $this->inject($this->subject, 'eventgroups', $eventgroupsObjectStorageMock);

        $this->subject->addEventgroup($eventgroup);
    }

    /**
     * @test
     */
    public function removeEventgroupFromObjectStorageHoldingEventgroups()
    {
        $eventgroup = new \KapelanMedien\KmTimeline\Domain\Model\Eventgroup();
        $eventgroupsObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', ['detach'], [], '', false);
        $eventgroupsObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($eventgroup));
        $this->inject($this->subject, 'eventgroups', $eventgroupsObjectStorageMock);

        $this->subject->removeEventgroup($eventgroup);

    }
}
