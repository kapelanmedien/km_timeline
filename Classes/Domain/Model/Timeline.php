<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Domain\Model;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use KapelanMedien\KmTimeline\Domain\Model\Era;
use KapelanMedien\KmTimeline\Domain\Model\Event;
use KapelanMedien\KmTimeline\Domain\Model\Eventgroup;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Timeline of a chronicle.
 */
class Timeline extends AbstractEntity
{

    /**
     * hidden
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * Name of the timeline.
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $name = '';

    /**
     * Description text of the timeline.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Title event of the timeline.
     *
     * @var Event
     */
    protected $title = null;

    /**
     * Events of the timeline.
     *
     * @var ObjectStorage<Event>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $events = null;

    /**
     * Eras of the timeline.
     *
     * @var ObjectStorage<Era>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $eras = null;

    /**
     * Event groups of the timeline.
     *
     * @var ObjectStorage<Eventgroup>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $eventgroups = null;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     *
     * @return void
     */
    protected function initStorageObjects(): void
    {
        $this->events = new ObjectStorage();
        $this->eras = new ObjectStorage();
        $this->eventgroups = new ObjectStorage();
    }

    /**
     * Returns the name
     *
     * @return string name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * Returns the description
     *
     * @return string description
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * Returns the title
     *
     * @return Event|null title
     */
    public function getTitle(): ?Event
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param Event $title
     * @return void
     */
    public function setTitle(Event $title): void
    {
        $this->title = $title;
    }

    /**
     * Adds a Event
     *
     * @param Event $event
     * @return void
     */
    public function addEvent(Event $event): void
    {
        $this->events->attach($event);
    }

    /**
     * Removes a Event
     *
     * @param Event $eventToRemove The Event to be removed
     * @return void
     */
    public function removeEvent(Event $eventToRemove): void
    {
        $this->events->detach($eventToRemove);
    }

    /**
     * Returns the events
     *
     * @return Event[] events
     */
    public function getEvents(): array
    {
//         return $this->events;
        return $this->events->toArray();
    }

    /**
     * Sets the events
     *
     * @param ObjectStorage<Event> $events
     * @return void
     */
    public function setEvents(ObjectStorage $events): void
    {
        $this->events = $events;
    }

    /**
     * Adds a Era
     *
     * @param Era $era
     * @return void
     */
    public function addEra(Era $era): void
    {
        $this->eras->attach($era);
    }

    /**
     * Removes a Era
     *
     * @param Era $eraToRemove The Era to be removed
     * @return void
     */
    public function removeEra(Era $eraToRemove): void
    {
        $this->eras->detach($eraToRemove);
    }

    /**
     * Returns the eras
     *
     * @return Era[] eras
     */
    public function getEras(): array
    {
//         return $this->eras;
        return $this->eras->toArray();
    }

    /**
     * Sets the eras
     *
     * @param ObjectStorage<Era> $eras
     * @return void
     */
    public function setEras(ObjectStorage $eras): void
    {
        $this->eras = $eras;
    }

    /**
     * Adds a Eventgroup
     *
     * @param Eventgroup $eventgroup
     * @return void
     */
    public function addEventgroup(Eventgroup $eventgroup): void
    {
        $this->eventgroups->attach($eventgroup);
    }

    /**
     * Removes a Eventgroup
     *
     * @param Eventgroup $eventgroupToRemove The Eventgroup to be removed
     * @return void
     */
    public function removeEventgroup(Eventgroup $eventgroupToRemove): void
    {
        $this->eventgroups->detach($eventgroupToRemove);
    }

    /**
     * Returns the eventgroups
     *
     * @return ObjectStorage<Eventgroup> $eventgroups
     */
    public function getEventgroups(): ObjectStorage
    {
        return $this->eventgroups;
    }

    /**
     * Sets the eventgroups
     *
     * @param ObjectStorage<Eventgroup> $eventgroups
     * @return void
     */
    public function setEventgroups(ObjectStorage $eventgroups): void
    {
        $this->eventgroups = $eventgroups;
    }

    /**
     * Returns hidden
     *
     * @return bool $hidden
     */
    public function getHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * Sets hidden
     *
     * @param bool $hidden
     * @return void
     */
    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns the bool state of hidden
     *
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->getHidden();
    }

}
