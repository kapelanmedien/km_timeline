<?php

namespace KapelanMedien\KmTimeline\Tests\Unit\Domain\Model;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case for class \KapelanMedien\KmTimeline\Domain\Model\Event.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Uwe Wiebach <wiebach@kapelan.com>
 */
class EventTest extends UnitTestCase
{
    /**
     * @var \KapelanMedien\KmTimeline\Domain\Model\Event
     */
    protected $subject = null;

    public function setUp()
    {
        $this->subject = new \KapelanMedien\KmTimeline\Domain\Model\Event();
    }

    public function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function getHeadlineReturnsInitialValueForString()
    {
        $this->assertSame(
            '',
            $this->subject->getHeadline()
        );
    }

    /**
     * @test
     */
    public function setHeadlineForStringSetsHeadline()
    {
        $this->subject->setHeadline('Conceived at T3CON10');

        $this->assertAttributeEquals(
            'Conceived at T3CON10',
            'headline',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        $this->assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        $this->assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStartYearReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setStartYearForIntSetsStartYear()
    {    }

    /**
     * @test
     */
    public function getStartMonthReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setStartMonthForIntSetsStartMonth()
    {    }

    /**
     * @test
     */
    public function getStartDayReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setStartDayForIntSetsStartDay()
    {    }

    /**
     * @test
     */
    public function getStartTimeReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setStartTimeForIntSetsStartTime()
    {    }

    /**
     * @test
     */
    public function getEndYearReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setEndYearForIntSetsEndYear()
    {    }

    /**
     * @test
     */
    public function getEndMonthReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setEndMonthForIntSetsEndMonth()
    {    }

    /**
     * @test
     */
    public function getEndDayReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setEndDayForIntSetsEndDay()
    {    }

    /**
     * @test
     */
    public function getEndTimeReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setEndTimeForIntSetsEndTime()
    {    }

    /**
     * @test
     */
    public function getDateFormatReturnsInitialValueForInt()
    {    }

    /**
     * @test
     */
    public function setDateFormatForIntSetsDateFormat()
    {    }

    /**
     * @test
     */
    public function getMediaUrlReturnsInitialValueForString()
    {
        $this->assertSame(
            '',
            $this->subject->getMediaUrl()
        );
    }

    /**
     * @test
     */
    public function setMediaUrlForStringSetsMediaUrl()
    {
        $this->subject->setMediaUrl('Conceived at T3CON10');

        $this->assertAttributeEquals(
            'Conceived at T3CON10',
            'mediaUrl',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEventgroupReturnsInitialValueForEventgroup()
    {
        $this->assertEquals(
            null,
            $this->subject->getEventgroup()
        );
    }

    /**
     * @test
     */
    public function setEventgroupForEventgroupSetsEventgroup()
    {
        $eventgroupFixture = new \KapelanMedien\KmTimeline\Domain\Model\Eventgroup();
        $this->subject->setEventgroup($eventgroupFixture);

        $this->assertAttributeEquals(
            $eventgroupFixture,
            'eventgroup',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMediaReturnsInitialValueForFileReference()
    {
        $this->assertEquals(
            null,
            $this->subject->getMedia()
        );
    }

    /**
     * @test
     */
    public function setMediaForFileReferenceSetsMedia()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setMedia($fileReferenceFixture);

        $this->assertAttributeEquals(
            $fileReferenceFixture,
            'media',
            $this->subject
        );
    }
}
