<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "km_timeline"
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Timeline',
    'description' => 'Creates a timeline of a chronicle.',
    'category' => 'plugin',
    'state' => 'stable',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'Uwe Wiebach',
    'author_email' => 'wiebach@kapelan.com',
    'author_company' => 'Kapelan Medien GmbH',
    'version' => '1.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'KapelanMedien\\KmTimeline\\' => 'Classes'
        ]
    ]
];
