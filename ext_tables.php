<?php
defined('TYPO3') or die();

(function () {
    /**
     * Registers a Backend Module
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'KmTimeline',
        'web',
        'Timelines',
        '',
        [
//             \KapelanMedien\KmTimeline\Controller\TimelineController::class => 'list, show',
            \KapelanMedien\KmTimeline\Controller\TimelineController::class => 'list',
        ],
        [
            'access' => 'user,group',
            'iconIdentifier' => 'km-timeline',
            'labels' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_be.xlf',
        ]
    );
    
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kmtimeline_domain_model_timeline', 'EXT:km_timeline/Resources/Private/Language/locallang_csh_tx_kmtimeline_domain_model_timeline.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kmtimeline_domain_model_timeline');
    
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kmtimeline_domain_model_event', 'EXT:km_timeline/Resources/Private/Language/locallang_csh_tx_kmtimeline_domain_model_event.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kmtimeline_domain_model_event');
    
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kmtimeline_domain_model_eventgroup', 'EXT:km_timeline/Resources/Private/Language/locallang_csh_tx_kmtimeline_domain_model_eventgroup.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kmtimeline_domain_model_eventgroup');
    
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kmtimeline_domain_model_era', 'EXT:km_timeline/Resources/Private/Language/locallang_csh_tx_kmtimeline_domain_model_era.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kmtimeline_domain_model_era');
})();
