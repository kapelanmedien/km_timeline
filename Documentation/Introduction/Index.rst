﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============


.. _what-it-does:

What does it do?
----------------

This extension creates a timeline for the events of a chronicle. Although you can define which JavaScript plugin
you want to use, TimelineJS is recommended and the whole data model is tailor-made for it.


.. _screenshots:

Screenshots
-----------

This chapter should help people figure how the extension works. Remove it
if not relevant.

.. figure:: ../Images/IntroductionPackage.png
   :width: 500px
   :alt: Introduction Package

   Introduction Package just after installation (caption of the image)

   How the Frontend of the Introduction Package looks like just after installation (legend of the image)
