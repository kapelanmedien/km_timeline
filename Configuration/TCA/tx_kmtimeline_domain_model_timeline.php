<?php

return [
    'ctrl' => [
        'title'    => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_timeline',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => true,

        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'name,description,title,events,eras,eventgroups,',
        'iconfile' => 'EXT:km_timeline/Resources/Public/Icons/tx_kmtimeline_domain_model_timeline.svg'
    ],
    'interface' => [
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, description, title, events, eras, eventgroups, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [

        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kmtimeline_domain_model_timeline',
                'foreign_table_where' => 'AND tx_kmtimeline_domain_model_timeline.pid=###CURRENT_PID### AND tx_kmtimeline_domain_model_timeline.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],
        'starttime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
            ],
        ],
        'endtime' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'range' => [
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ],
            ],
        ],

        'name' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_timeline.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_timeline.description',
            'config' => [
                'type' => 'text',
                'cols' => 48,
                'rows' => 5,
                'eval' => 'trim',
                'enableRichtext' => true,
            ],
        ],
        'title' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_timeline.title',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kmtimeline_domain_model_event',
                'minitems' => 0,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'events' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_timeline.events',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kmtimeline_domain_model_event',
                'foreign_field' => 'timeline',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'eras' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_timeline.eras',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kmtimeline_domain_model_era',
                'foreign_field' => 'timeline',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],
        'eventgroups' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_timeline.eventgroups',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kmtimeline_domain_model_eventgroup',
                'foreign_field' => 'timeline',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],

        ],

    ],
];
