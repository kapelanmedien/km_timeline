<?php

namespace KapelanMedien\KmTimeline\Tests\Unit\Domain\Model;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case for class \KapelanMedien\KmTimeline\Domain\Model\Eventgroup.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Uwe Wiebach <wiebach@kapelan.com>
 */
class EventgroupTest extends UnitTestCase
{
    /**
     * @var \KapelanMedien\KmTimeline\Domain\Model\Eventgroup
     */
    protected $subject = null;

    public function setUp()
    {
        $this->subject = new \KapelanMedien\KmTimeline\Domain\Model\Eventgroup();
    }

    public function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        $this->assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        $this->assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }
}
