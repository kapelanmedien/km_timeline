<?php
namespace KapelanMedien\KmTimeline\Tests\Unit\Controller;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use KapelanMedien\KmTimeline\Controller\TimelineController;

/**
 * Test case for class KapelanMedien\KmTimeline\Controller\TimelineController.
 *
 * @author Uwe Wiebach <wiebach@kapelan.com>
 */
class TimelineControllerTest extends UnitTestCase
{

    /**
     * @var TimelineController
     */
    protected $subject = null;

    public function setUp()
    {
        $this->subject = $this->getMock('KapelanMedien\\KmTimeline\\Controller\\TimelineController', ['redirect', 'forward', 'addFlashMessage'], [], '', false);
    }

    public function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @test
     */
    public function listActionFetchesAllTimelinesFromRepositoryAndAssignsThemToView()
    {

        $allTimelines = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', [], [], '', false);

        $timelineRepository = $this->getMock('KapelanMedien\\KmTimeline\\Domain\\Repository\\TimelineRepository', ['findAll'], [], '', false);
        $timelineRepository->expects($this->once())->method('findAll')->will($this->returnValue($allTimelines));
        $this->inject($this->subject, 'timelineRepository', $timelineRepository);

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $view->expects($this->once())->method('assign')->with('timelines', $allTimelines);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenTimelineToView()
    {
        $timeline = new \KapelanMedien\KmTimeline\Domain\Model\Timeline();

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $this->inject($this->subject, 'view', $view);
        $view->expects($this->once())->method('assign')->with('timeline', $timeline);

        $this->subject->showAction($timeline);
    }
}
