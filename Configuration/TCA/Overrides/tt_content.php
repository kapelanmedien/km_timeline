<?php
defined('TYPO3') or die();

/***************
 * Plugin
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'KapelanMedien.km_timeline',
    'TimelineJs',
    'LLL:EXT:km_timeline/Resources/Private/Language/locallang_be.xlf:plugin_title'
);

$pluginSignature = 'kmtimeline_timelinejs';

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:km_timeline/Configuration/FlexForms/flexform_timelinejs.xml');
