﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Timeline
=============================================================

.. only:: html

	:Classification:
		km_timeline

	:Version:
		|release|

	:Language:
		en

	:Description:
		Creates a timeline of a chronicle.

	:Keywords:
		timeline,chronicle

	:Copyright:
		2016

	:Author:
		Uwe Wiebach

	:Email:
		wiebach@kapelan.com

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Links
