<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Domain\Model;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Era
 */
class Era extends AbstractEntity
{

    /**
     * Title of the era.
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $headline = '';

    /**
     * Year in which the era started.
     *
     * @var int
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $startYear = null;

    /**
     * Month in which the era started.
     *
     * @var int
     */
    protected $startMonth = 0;

    /**
     * Day in which the era started.
     *
     * @var int
     */
    protected $startDay = 0;

    /**
     * Time in which the era started.
     *
     * @var \DateTime
     */
    protected $startTime = null;

    /**
     * Year in which the era ended.
     *
     * @var int
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $endYear = null;

    /**
     * Month in which the era ended.
     *
     * @var int
     */
    protected $endMonth = 0;

    /**
     * Day in which the era ended.
     *
     * @var int
     */
    protected $endDay = 0;

    /**
     * Time in which the era ended.
     *
     * @var \DateTime
     */
    protected $endTime = null;

    /**
     * Date format for display.
     *
     * @var int
     */
    protected $dateFormat = 0;

    /**
     * Returns the headline
     *
     * @return string $headline
     */
    public function getHeadline(): string
    {
        return $this->headline;
    }

    /**
     * Sets the headline
     *
     * @param string $headline
     * @return void
     */
    public function setHeadline($headline): void
    {
        $this->headline = $headline;
    }

    /**
     * Returns the startYear
     *
     * @return int startYear
     */
    public function getStartYear(): int
    {
        return $this->startYear;
    }

    /**
     * Sets the startYear
     *
     * @param int $startYear
     * @return void
     */
    public function setStartYear($startYear): void
    {
        $this->startYear = $startYear;
    }

    /**
     * Returns the startMonth
     *
     * @return int $startMonth
     */
    public function getStartMonth(): int
    {
        return $this->startMonth;
    }

    /**
     * Sets the startMonth
     *
     * @param int $startMonth
     * @return void
     */
    public function setStartMonth($startMonth): void
    {
        $this->startMonth = $startMonth;
    }

    /**
     * Returns the startDay
     *
     * @return int $startDay
     */
    public function getStartDay(): int
    {
        return $this->startDay;
    }

    /**
     * Sets the startDay
     *
     * @param int $startDay
     * @return void
     */
    public function setStartDay($startDay): void
    {
        $this->startDay = $startDay;
    }

    /**
     * Returns the startTime
     *
     * @return \DateTime|null $startTime
     */
    public function getStartTime(): ?\DateTime
    {
        $startTimeTZ = $this->startTime;
        if ($startTimeTZ != null) {
            $startTimeTZ->setTimezone(new \DateTimeZone('UTC'));
        }
        
        return $startTimeTZ;
//         return $this->startTime;
    }

    /**
     * Sets the startTime
     *
     * @param \DateTime $startTime
     * @return void
     */
    public function setStartTime(\DateTime $startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * Returns the endYear
     *
     * @return int endYear
     */
    public function getEndYear(): int
    {
        return $this->endYear;
    }

    /**
     * Sets the endYear
     *
     * @param int $endYear
     * @return void
     */
    public function setEndYear($endYear): void
    {
        $this->endYear = $endYear;
    }

    /**
     * Returns the endMonth
     *
     * @return int $endMonth
     */
    public function getEndMonth(): int
    {
        return $this->endMonth;
    }

    /**
     * Sets the endMonth
     *
     * @param int $endMonth
     * @return void
     */
    public function setEndMonth($endMonth): void
    {
        $this->endMonth = $endMonth;
    }

    /**
     * Returns the endDay
     *
     * @return int $endDay
     */
    public function getEndDay(): int
    {
        return $this->endDay;
    }

    /**
     * Sets the endDay
     *
     * @param int $endDay
     * @return void
     */
    public function setEndDay($endDay): void
    {
        $this->endDay = $endDay;
    }

    /**
     * Returns the endTime
     *
     * @return \DateTime|null $endTime
     */
    public function getEndTime(): ?\DateTime
    {
        $endTimeTZ = $this->endTime;
        if ($endTimeTZ != null) {
            $endTimeTZ->setTimezone(new \DateTimeZone('UTC'));
        }
        
        return $endTimeTZ;
//         return $this->endTime;
    }

    /**
     * Sets the endTime
     *
     * @param \DateTime $endTime
     * @return void
     */
    public function setEndTime(\DateTime $endTime): void
    {
        $this->endTime = $endTime;
    }

    /**
     * Returns the dateFormat
     *
     * @return int $dateFormat
     */
    public function getDateFormat(): int
    {
        return $this->dateFormat;
    }

    /**
     * Sets the dateFormat
     *
     * @param int $dateFormat
     * @return void
     */
    public function setDateFormat($dateFormat): void
    {
        $this->dateFormat = $dateFormat;
    }

}
