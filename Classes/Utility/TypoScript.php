<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Utility;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * TypoScript Utility class
 */
class TypoScript
{

    /**
     * @param array $base
     * @param array $overload
     * @return array
     */
    public function override(array $base, array $overload): array
    {
        foreach ($overload as $fieldName => $fieldValue) {
            // Multilevel field
            if (is_array($fieldValue) !== false) {
                if (isset($base[$fieldName])) {
                    $base[$fieldName] = $this->override($base[$fieldName], $fieldValue);
                } else {
                    $base[$fieldName] = $fieldValue;
                }
            } else {
                // set flexform setting if not 0 or empty
                if ($fieldValue !== '0' && strlen($fieldValue) > 0) {
                    $base[$fieldName] = $fieldValue;
                }
            }
        }
        return $base;
    }
}
