<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Domain\Repository;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;
use KapelanMedien\KmTimeline\Domain\Model\Timeline;

/**
 * The repository for Timelines
 */
class TimelineRepository extends Repository
{

    protected $defaultOrderings = [
        'name' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Returns all objects of this repository.
     *
     * @api
     * @return QueryResultInterface|Timeline[]
     */
    public function findAll(): QueryResultInterface|array
    {
        $query = $this->createQuery();
        // show hidden records in BE mode
        if (ApplicationType::fromRequest(self::getRequest())->isBackend()) {
            $querySettings = $query->getQuerySettings();
            $querySettings->setEnableFieldsToBeIgnored(['hidden'])->setIgnoreEnableFields(true);
            $query->setQuerySettings($querySettings);
        }
        return $query->execute();
    }
    
    /**
     * @return ServerRequestInterface Request object
     */
    private static function getRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }

}
