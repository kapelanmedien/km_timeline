<?php
declare(strict_types=1);

namespace KapelanMedien\KmTimeline\Domain\Repository;

/*
 * This file is part of the "km_timeline" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;
use KapelanMedien\KmTimeline\Domain\Model\Era;

/**
 * The repository for Eras
 */
class EraRepository extends Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING
    ];
    
    /**
     * @param integer $timeline
     * @param integer $maxEntries
     * @return QueryResultInterface|Era[]
     */
    public function findByTimeline($timeline, $maxEntries = 0): QueryResultInterface|array
    {
        $query = $this->createQuery();
        $querySettings = $query->getQuerySettings();
        $querySettings->setRespectSysLanguage(false);
        // show hidden records in BE mode
        if (ApplicationType::fromRequest(self::getRequest())->isBackend()) {
            $querySettings->setEnableFieldsToBeIgnored(['hidden'])->setIgnoreEnableFields(true);
        }
        $query->setQuerySettings($querySettings);
        $query->matching($query->equals('timeline', $timeline));
        if (intval($maxEntries) > 0) {
            $query->setLimit(intval($maxEntries));
        }
        return $query->execute();
    }
    
    /**
     * @return ServerRequestInterface Request object
     */
    private static function getRequest(): ServerRequestInterface
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }

}
