<?php

return [
    'ctrl' => [
        'title' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era',
        'label' => 'headline',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'versioningWS' => true,

        'languageField' => 'sys_language_uid',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'headline,start_year,start_month,start_day,start_time,end_year,end_month,end_day,end_time,date_format,',
        'iconfile' => 'EXT:km_timeline/Resources/Public/Icons/tx_kmtimeline_domain_model_era.svg'
    ],
    'interface' => [
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, headline, --palette--;LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.start;2, --palette--;LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.end;3, date_format, '],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
        '2' => ['showitem' => 'start_year, start_month, start_day, start_time'],
        '3' => ['showitem' => 'end_year, end_month, end_day, end_time'],
    ],
    'columns' => [

        'sys_language_uid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kmtimeline_domain_model_era',
                'foreign_table_where' => 'AND tx_kmtimeline_domain_model_era.pid=###CURRENT_PID### AND tx_kmtimeline_domain_model_era.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],

        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],

        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
            ],
        ],

        'headline' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.headline',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'start_year' => [
            'exclude' => 0,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.start_year',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int,required',
                'max' => 4,
                'default' => date('Y')
            ]
        ],
        'start_month' => [
            'exclude' => 0,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.start_month',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.1', 1],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.2', 2],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.3', 3],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.4', 4],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.5', 5],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.6', 6],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.7', 7],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.8', 8],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.9', 9],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.10', 10],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.11', 11],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.12', 12],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'int',
                'default' => date('n')
            ],
        ],
        'start_day' => [
            'exclude' => 0,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.start_day',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                    ['1', 1],
                    ['2', 2],
                    ['3', 3],
                    ['4', 4],
                    ['5', 5],
                    ['6', 6],
                    ['7', 7],
                    ['8', 8],
                    ['9', 9],
                    ['10', 10],
                    ['11', 11],
                    ['12', 12],
                    ['13', 13],
                    ['14', 14],
                    ['15', 15],
                    ['16', 16],
                    ['17', 17],
                    ['18', 18],
                    ['19', 19],
                    ['20', 20],
                    ['21', 21],
                    ['22', 22],
                    ['23', 23],
                    ['24', 24],
                    ['25', 25],
                    ['26', 26],
                    ['27', 27],
                    ['28', 28],
                    ['29', 29],
                    ['30', 30],
                    ['31', 31],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'int',
                'default' => date('j')
            ],
        ],
        'start_time' => [
            'exclude' => 1,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.start_time',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 4,
                'eval' => 'null,time,int',
                'default' => time()
            ]
        ],
        'end_year' => [
            'exclude' => 0,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.end_year',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int,required',
                'max' => 4,
                'default' => date('Y')
            ]
        ],
        'end_month' => [
            'exclude' => 0,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.end_month',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.1', 1],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.2', 2],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.3', 3],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.4', 4],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.5', 5],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.6', 6],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.7', 7],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.8', 8],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.9', 9],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.10', 10],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.11', 11],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:month.12', 12],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'int',
                'default' => date('n')
            ],
        ],
        'end_day' => [
            'exclude' => 0,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.end_day',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                    ['1', 1],
                    ['2', 2],
                    ['3', 3],
                    ['4', 4],
                    ['5', 5],
                    ['6', 6],
                    ['7', 7],
                    ['8', 8],
                    ['9', 9],
                    ['10', 10],
                    ['11', 11],
                    ['12', 12],
                    ['13', 13],
                    ['14', 14],
                    ['15', 15],
                    ['16', 16],
                    ['17', 17],
                    ['18', 18],
                    ['19', 19],
                    ['20', 20],
                    ['21', 21],
                    ['22', 22],
                    ['23', 23],
                    ['24', 24],
                    ['25', 25],
                    ['26', 26],
                    ['27', 27],
                    ['28', 28],
                    ['29', 29],
                    ['30', 30],
                    ['31', 31],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'int',
                'default' => date('j')
            ],
        ],
        'end_time' => [
            'exclude' => 1,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.end_time',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 4,
                'eval' => 'null,time,int',
                'default' => time()
            ]
        ],
        'date_format' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:tx_kmtimeline_domain_model_era.date_format',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.1', 1],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.2', 2],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.3', 3],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.4', 4],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.5', 5],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.6', 6],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.7', 7],
//                     ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.8', 8],
//                     ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.9', 9],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.10', 10],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.11', 11],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.12', 12],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.13', 13],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.14', 14],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.15', 15],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.16', 16],
                    ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.17', 17],
//                     ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.18', 18],
//                     ['LLL:EXT:km_timeline/Resources/Private/Language/locallang_db.xlf:date_format.19', 19],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'int'
            ],
        ],

        'timeline' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
